import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ProjectItem from './ProjectItem';

class Projects extends Component {

    deleteItem(id) {

        this.props.onDelete(id);
    }

    render() {
        let items;
        if(this.props.projects) {
            items = this.props.projects.map(project => {
                return (
                    <ProjectItem onDelete={this.deleteItem.bind(this)} key={project.title} project={project} />
                );
            });
        }
        return (
            <div className="Projects">
                {items} 
            </div>
        );
    }
}

Projects.propTypes = {    
    projects:PropTypes.array,
    onDelete:PropTypes.func
}

export default Projects;