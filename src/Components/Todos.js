import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TodoItem from './TodoItem';

class Todos extends Component {
    render() {
        let items;
        if(this.props.todos) {
            items = this.props.todos.map(todo => {
                return (
                    <TodoItem key={todo.title} todo={todo} />
                );
            });
        }
        return (
            <div className="Todos">
                {items} 
            </div>
        );
    }
}

Todos.propTypes = {    
    todos:PropTypes.array
}

export default Todos;